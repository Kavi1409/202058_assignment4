FROM gcc
MAINTAINER Kavisha Perera
RUN apt-get update && apt-get install -y \
vim \
gdb \
git
RUN git config --global user.email "kavishaperera1409@gmail.com"
RUN git config --global user.name "Kavi1409"
