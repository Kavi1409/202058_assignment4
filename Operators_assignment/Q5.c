//Q5. a c program to demonstrate the given operators using the same program
//A= 10 and B=15
//bitwise operators: A&B, A^B, ~A, A<<3, B>>3 

#include <stdio.h>

int main (){

	int A=10, B=15;

	printf("\nDemonstrating bitwise operators!\n");
	printf("A=10 and B=15\n");
	
	printf("\n");

	printf("Bitwise AND operator > A&B > %d\n",A&B);
	printf("Bitwise XOR operator > A^B > %d\n",A^B);
	printf("Bitwise Complement operator > ~A > %d\n",~A);
	printf("Bitwise Left Shift Operator >\n");
		printf("	Left Shift A by %d (A<<3): %d\n", 3, A<<3);
	printf("Bitwise Right Shift Operator >\n");
		printf("	Right Shift B by %d (B>>3): %d\n", 3, B>>3);

	printf("\n");

	return 0;
}
