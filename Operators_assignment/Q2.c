// Q2. a c program to find the volume of a cone where the user inputs the height and radius of the cone
//

#include <stdio.h>

int main (){

	float r, h; // *r* is radius and *h* is height
	float pi = 3.14;
	float volume;

	printf("Enter the radius of the cone: ");
	scanf("%f",&r);
	printf("Enter the height of the cone: ");
	scanf("%f",&h);

	volume = pi*(r*r)*h/3;

	printf("The volume of this cone is: %f\n",volume);

	return 0;
}



	
