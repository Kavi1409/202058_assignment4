// Q3. a c program to swap two numbers without using a temporary variable
//
#include <stdio.h>

int main (){

	int a,b;
	printf("Give two numbers to swap!\n");
	
	printf("Number A: ");
	scanf("%d",&a);

	printf("Number B: ");
	scanf("%d",&b);
	
	//eg: if a=5 and b=10

	a=a+b; //here a+b = 5+10  = 15
	b=a-b; //here a-b = 15-10 = 5
	a=a-b; //here a-b = 15-5  = 10  //Therefore a has become 10, while b has become 5!

	printf("The two numbers after swapping >\n");
	printf("Number A: %d\n",a);
	printf("Number B: %d\n",b);

	return 0;
}





