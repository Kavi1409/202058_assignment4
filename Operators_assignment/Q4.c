// Q4. a c program to convert celcius to fahrenheit
//
#include <stdio.h>

int main (){

	float tempC, tempF;
	printf("Give me the temperature in Celcius: ");
	scanf("%f",&tempC);

	tempF= (tempC * 9/5) + 32;

	printf("The given temperature in Fahrenheit: %.2f\n",tempF);

	return 0;
}

	
